const authHelper = require('../../../helper/auth');

const callback = (req, res) => {
    console.log(req.query);
    const code = req.query.code;

    if (code) {
        console.log(code);
        res.redirect(process.env.SUCCESS_CALLBACK_URL);
    } else {
        res.redirect(process.env.ERROR_CALLBACK_URL);
    }
};
module.exports = callback;

