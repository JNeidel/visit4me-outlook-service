const { Router } = require('express');

const getAuthURL = require('./post/getAuthURL');
const callback = require('./get/callback');

const auth = Router();

auth.get('/callback', callback);
auth.post('/getAuthURL', getAuthURL);

module.exports = auth;