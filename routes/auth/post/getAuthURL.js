const authHelper = require('../../../helper/auth');

const getAuthUrl = (req, res) => {
    const url = authHelper.getAuthUrl();
    res.json({ url });
};
module.exports = getAuthUrl;