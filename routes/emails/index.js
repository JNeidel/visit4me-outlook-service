const { Router } = require('express');

const getEmails = require('./get/getEmails');

const emails = Router();

emails.get('/', getEmails);

module.exports = emails;
