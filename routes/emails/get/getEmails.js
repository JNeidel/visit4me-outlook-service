const graph = require('@microsoft/microsoft-graph-client');

const getEmails = async (req, res) => {
  const accessToken = 'OAQABAAIAAACEfexXxjamQb3OeGQ4GugvcaREQgWvooOO3prU2KtM_NJZEYtyG3PWif-IlN8XFYZEADmMXEYCoJlD0lF1CiBDM3sjEP9FCHHePiUKcHUMzb7LqX8Vm7ffCbgfJTGihKs3Ux_y_7taE0mpEPj3FwB1uReL8s--wnzEJVOdQkSp9FQ2PaL_lVMUfUL4ymF-Y-NyNXxber-SKsgp6Y8ADoBbaVatDrfZjFyTCo9WSuSMah_ZC7CR1xARC0OpdrSCJDWnMrFlKFTcEyPbs46-7tK_TSHZ-KrHCCjoRLd3A579PLYX0XG7Uvtk5MsbMDTG_lYw2KHIVfjAjmD6mLSn2k9PkCZ9qz-vHCPy0PD76C4-FJuwIDu4E-w3Fp3RTWgtqwRpND00cwf0PZCS4vG5jZuyJMQ0S0fKzsTr0goIfg6Hzg_vnBY7mMQ8RwEj1wMB738j3p2p9Obf9Uow7DtWKF6iGDWO9UYr77XAzQ3CwEq4ifH5lqITGVrZoF0Qfon0Ch6FqS3bfGDb7UpVklg6Z4n_jy3bPFU8Xel_UzApnyfZzk7iUiBsmtprKKPSvwyUoIzVgLdUNpEIU4ltY1bOm6pBtfbHFHXwz4HT75cGmUoilY1INIyODLH6iGqUDTDGSP2aCbbjrXmCGt2tbX1Ne0uzzHGeKv5M4PXuD9ehEWp2Wco53OX3wpp_ri-Z6he7_1AgAA';
  const userName = '';
  const client = graph.Client.init({
    authProvider: (done) => {
      done(null, accessToken);
    }
  });

  const result = await client
    .api('/me/mailfolders/inbox/messages')
    .top(10)
    .select('subject,from,receivedDateTime,isRead')
    .orderby('receivedDateTime DESC')
    .get();

  res.json(result);
};
module.exports = getEmails;
